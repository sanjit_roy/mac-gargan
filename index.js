const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const PORT = 9999;

const app = express();
//npk3i7kszXacHq4
const db = mongoose.connection;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( {
    extended: true
}));
app.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();

});

function init() {
    mongoose.connect('mongodb+srv://roysanjit:npk3i7kszXacHq4@cluster0-sjcou.mongodb.net/test?retryWrites=true&w=majority', 
    {   
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    db.on('error', console.error.bind(console, 'database connection error...'));
    db.once('open', () => {
        console.log("Database connected...");
    });

    app.listen(PORT, () => console.log("Live at: ", PORT));
}

const HastasmDb = require('./model/hastasm.model.js');

app.get('/ping', (req, res) => {
    console.log("server ping requested...");
    res.status(200).json({'response': 'success'});
});
//TODO: Update Device Id in server.
app.get('/device/:device_id/settemperature/:current_temp.:unit', (req, res) => {
    HastasmDb.findOneAndUpdate({
        deviceId: req.params.device_id
    }, 
    {
        deviceId: req.params.device_id,
        'current.temperature.value': req.params.current_temp, 
        'current.temperature.unit': req.params.unit,
        'current.temperature.time_updated': Date.now()
    },
    {
        new: true,
        upsert: true
    }, (error, data) => {
        if (data) {
            console.log("Data: ");
            console.log(data + '\n');
            res.status(200).json(data);
        }
        if (error) {
            console.log(error);
            res.status(500).json(error);
        }
    });
    /**
    res.json({
        'temp': req.params.current_temp, 
        'unit': req.params.unit
    });
    */
});


app.get('/device/:device_id/sethumidity/:current_humidity.:unit', (req, res) => {
    HastasmDb.findOneAndUpdate({
        deviceId: req.params.device_id
    }, 
    {
        deviceId: req.params.device_id,
        'current.humidity.value': req.params.current_humidity, 
        'current.humidity.unit': req.params.unit,
        'current.humidity.time_updated': Date.now()
    },
    {
        new: true,
        upsert: true
    }, (error, data) => {
        if (data) {
            console.log("Data: ");
            console.log(data + '\n');
            res.status(200).json(data);
        }
        if (error) {
            console.log(error);
            res.status(500).json(error);
        }
    });
    /**
    res.json({
        'temp': req.params.current_temp, 
        'unit': req.params.unit
    });
    */
});

app.get("/device/:device_id/getweather", (req, res) => {
    HastasmDb.findOne({
        deviceId: req.params.device_id
    }, (error, data) => {
        if (error) {
            console.log(error);
            res.status(500).json(error);
        }
        if (data) {
            console.log("Data: ");
            console.log(data + '\n');
            res.status(200).json(data);
        }
    });
});

/**
 * Route path: /users/:userId/books/:bookId
Request URL: http://localhost:3000/users/34/books/8989
req.params: { "userId": "34", "bookId": "8989" }

app.get('/users/:userId/books/:bookId', function (req, res) {
  res.send(req.params)
})
 */

init(); //SHould be the last line of the server