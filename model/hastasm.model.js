const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let HastasmDbSchema = new Schema({
    deviceId: String,
    current: {
        temperature: {
            value: String,
            unit: String,
            time_updated: {
                type: String,
                default: Date.now()
            }
        },
        humidity: {
            value: String,
            unit: String,
            time_updated: {
                type: String,
                default: Date.now()
            }
        }
    }
});

module.exports = mongoose.model('HashtasmDb', HastasmDbSchema);